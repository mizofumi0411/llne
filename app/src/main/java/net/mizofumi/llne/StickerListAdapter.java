package net.mizofumi.llne;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.activeandroid.query.Select;

import net.mizofumi.llne.Storage.ImageManager;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by mizof on 2016/04/04.
 */
public class StickerListAdapter extends BaseAdapter {

    Context context;
    List<Stamp> stamps;
    LayoutInflater layoutInflater;

    public StickerListAdapter(Context context, List<Stamp> stamps) {
        this.context = context;
        this.stamps = stamps;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public List<Stamp> getStamps() {
        return stamps;
    }

    public void addStamps(Stamp stamp) {
        stamps.add(stamp);
    }

    @Override
    public int getCount() {
        return stamps.size();
    }

    @Override
    public Object getItem(int position) {
        return stamps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.stickerlist,parent,false);
        TextView title = (TextView)view.findViewById(R.id.title);
        TextView detail = (TextView)view.findViewById(R.id.detail);
        RatingBar rate = (RatingBar)view.findViewById(R.id.rate);
        rate.setRating(stamps.get(position).rate);
        ImageView preview = (ImageView)view.findViewById(R.id.preview);

        final List<StampImage> stampImages = new Select().from(StampImage.class).where("stampid = ?",stamps.get(0).getId()).execute();

        try {
            Bitmap bitmap = ImageManager.loadBitmap(context, stampImages.get(0).image);
            preview.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        title.setText(stamps.get(position).title);
        detail.setText(stamps.get(position).detail);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StampActivity.class);
                intent.putExtra("id", stamps.get(position).getId());
                context.startActivity(intent);
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                new AlertDialog.Builder(context)
                        .setTitle("確認")
                        .setMessage("削除してもよろしいですか？")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                stamps.get(position).delete();
                                stamps.remove(position);
                                notifyDataSetChanged();
                            }
                        }).show();
                return true;
            }
        });

        return view;
    }
}
