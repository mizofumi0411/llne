package net.mizofumi.llne;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.mizofumi.llne.Http.StickerGet;
import net.mizofumi.llne.Http.StickerImageGet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizof on 2016/04/05.
 */
public class StickerGetImpl extends Thread implements StickerGet,StickerImageGet {

    String url;
    StickerGet StickerGetListener;
    StickerImageGet StickerImageGetListener;

    public StickerGetImpl(String url) {
        this.url = url;
    }

    public StickerGetImpl(String url, StickerGet StickerGetListener,StickerImageGet StickerImageGetListener){
        this.url = url;
        this.StickerGetListener = StickerGetListener;
        this.StickerImageGetListener = StickerImageGetListener;
    }

    @Override
    public void onPreExec() {
        StickerGetListener.onPreExec();
    }

    @Override
    public void onPostExec(Stamp stamp) {
        StickerGetListener.onPostExec(stamp);
    }

    @Override
    public void doBackground(int now, int max) {
        StickerGetListener.doBackground(now, max);
    }

    @Override
    public void run() {
        super.run();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onPreExec();
            }
        });

        final Stamp stamp = new Stamp();
        final List<String> images = new ArrayList<>();
        stamp.source = url;
        try {
            Document document = Jsoup.connect(url)
                    .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36")
                    .get();
            stamp.title = document.getElementsByClass("mdCMN08Ttl").text();
            stamp.detail = document.getElementsByClass("mdCMN08Desc").text();
            stamp.rate = 0;
            Elements mdCMN09Image = document.getElementsByClass("mdCMN09Image");
            int now = 0;
            doBackground(0, mdCMN09Image.size());

            for (Element e : mdCMN09Image){
                doBackground(now, mdCMN09Image.size());

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(e.attr("style").split(" ")[4].replaceAll("background-image:url\\(", "").replaceAll("\\);", ""))
                        .build();
                Response response = client.newCall(request).execute();
                response.body().byteStream();
                Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                images.add(encoded);

                now++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onPostExec(stamp);
                onGet(images);
            }
        });
    }

    @Override
    public void onGet(List<String> images) {
        if (StickerImageGetListener != null) StickerImageGetListener.onGet(images);
    }
}
