package net.mizofumi.llne.Store;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.etsy.android.grid.StaggeredGridView;

import net.mizofumi.llne.Http.StickerGet;
import net.mizofumi.llne.Http.StickerGetDBImpl;
import net.mizofumi.llne.Http.StickerImageGet;
import net.mizofumi.llne.R;
import net.mizofumi.llne.Stamp;
import net.mizofumi.llne.StampListAdapter;
import net.mizofumi.llne.StickerGetImpl;

import java.util.List;

public class SampleActivity extends AppCompatActivity {

    private SampleStampListAdapter adapter;
    private MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("サンプル");
        setSupportActionBar(toolbar);

        final String url = getIntent().getStringExtra("url");

        View view = findViewById(R.id.contentView);
        final ImageView preview = (ImageView)view.findViewById(R.id.preview);
        final TextView title = (TextView)view.findViewById(R.id.title);
        final TextView detail = (TextView)view.findViewById(R.id.detail);
        StaggeredGridView gridView = (StaggeredGridView)view.findViewById(R.id.gridView);
        adapter = new SampleStampListAdapter(this);
        gridView.setAdapter(adapter);



        new StickerGetImpl(url, new StickerGet() {
            @Override
            public void onPreExec() {
                dialog = new MaterialDialog.Builder(SampleActivity.this)
                        .title("取得中")
                        .content("しばらくお待ちください")
                        .progress(true,0)
                        .cancelable(false)
                        .show();
            }

            @Override
            public void onPostExec(Stamp stamp) {
                title.setText(stamp.title);
                detail.setText(stamp.detail);
                dialog.dismiss();
            }

            @Override
            public void doBackground(int now, int max) {

            }
        }, new StickerImageGet() {
            @Override
            public void onGet(List<String> images) {
                preview.setImageBitmap(stringToBitmap(images.get(0)));
                for (String s : images){
                    adapter.addImage(s);
                    adapter.notifyDataSetChanged();
                }
            }
        }).start();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final MaterialDialog progressDialog = new MaterialDialog.Builder(SampleActivity.this)
                        .title("処理中")
                        .content("スタンプ情報を取得しています…")
                        .progress(false,100)
                        .show();

                StickerGetDBImpl stickerGet = new StickerGetDBImpl(SampleActivity.this,url, new StickerGet() {
                    @Override
                    public void onPreExec() {

                    }

                    @Override
                    public void onPostExec(final Stamp stamp) {
                        progressDialog.dismiss();
                        Snackbar.make(view, "スタンプ情報を更新しました", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }

                    @Override
                    public void doBackground(final int now, final int max) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                float p = ((float) now / (float) max) * 100;
                                progressDialog.setContent("スタンプ情報を取得しています…\n" + now + "/" + max);
                                progressDialog.setProgress((int) p);
                                Log.d("Progress", now + "/" + max);
                            }
                        });
                    }
                });
                stickerGet.start();
            }
        });

    }

    protected Bitmap stringToBitmap(String string){
        byte[] bytes = Base64.decode(string, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

}
