package net.mizofumi.llne.Store;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import net.mizofumi.llne.Http.StickerGet;
import net.mizofumi.llne.Http.StickerGetDBImpl;
import net.mizofumi.llne.R;
import net.mizofumi.llne.Stamp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizof on 2016/04/05.
 */
public class StoreStampsAdapter extends BaseAdapter {

    Context context;
    LayoutInflater layoutInflater;
    List<Stamp> stamps = new ArrayList<>();

    public StoreStampsAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public List<Stamp> getStamps() {
        return stamps;
    }

    public void addStamsp(Stamp stamp){
        this.stamps.add(stamp);
    }

    @Override
    public int getCount() {
        return stamps.size();
    }

    @Override
    public Object getItem(int position) {
        return stamps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View view = layoutInflater.inflate(R.layout.storestamp,parent,false);
        ImageView preview = (ImageView)view.findViewById(R.id.preview);
        TextView label = (TextView)view.findViewById(R.id.label);
        TextView title = (TextView)view.findViewById(R.id.title);

        title.setText(stamps.get(position).title);


        if (stamps.get(position).createtype.equals("general"))
            label.setText("公式");

        if (stamps.get(position).createtype.equals("creater"))
            label.setText("クリエイター");

        byte[] bytes = Base64.decode(stamps.get(position).preview, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        preview.setImageBitmap(decodedByte);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SampleActivity.class);
                intent.putExtra("url", stamps.get(position).source);
                context.startActivity(intent);
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final MaterialDialog progressDialog = new MaterialDialog.Builder(context)
                        .title("処理中")
                        .content("スタンプ情報を取得しています…")
                        .progress(false,100)
                        .show();

                StickerGetDBImpl stickerGet = new StickerGetDBImpl(context,stamps.get(position).source, new StickerGet() {
                    @Override
                    public void onPreExec() {

                    }

                    @Override
                    public void onPostExec(final Stamp stamp) {
                        progressDialog.dismiss();
                        Snackbar.make(view, "スタンプ情報を更新しました", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }

                    @Override
                    public void doBackground(final int now, final int max) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                float p = ((float) now / (float) max) * 100;
                                progressDialog.setContent("スタンプ情報を取得しています…\n" + now + "/" + max);
                                progressDialog.setProgress((int) p);
                                Log.d("Progress", now + "/" + max);
                            }
                        });
                    }
                });
                stickerGet.start();
                return true;
            }
        });

        return view;
    }
}
