package net.mizofumi.llne.Store;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.etsy.android.grid.StaggeredGridView;

import net.mizofumi.llne.Http.StickersList;
import net.mizofumi.llne.Http.StickersListImpl;
import net.mizofumi.llne.R;
import net.mizofumi.llne.Stamp;

import java.util.List;

public class StoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        View view = (findViewById(R.id.contentView));
        StaggeredGridView stickers = (StaggeredGridView)view.findViewById(R.id.stickers);
        stickers.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount != 0 && totalItemCount == firstVisibleItem + visibleItemCount) {
                    // 最後尾までスクロールしたので、何かデータ取得する処理
                }
            }
        });
        final StoreStampsAdapter adapter = new StoreStampsAdapter(this);
        stickers.setAdapter(adapter);

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("取得中")
                .content("しばらくお待ちください")
                .progress(true,0)
                .cancelable(false)
                .show();

        StickersListImpl stickersList = new StickersListImpl(this, new StickersList() {
            @Override
            public void getStickers(List<Stamp> stamps) {
                for (Stamp stamp : stamps){
                    adapter.getStamps().add(stamp);
                }
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        stickersList.start();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            int select = 0;
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(StoreActivity.this)
                        .title("検索")
                        .content("キーワードを入力してください")
                        .inputType(InputType.TYPE_CLASS_TEXT)
                        .input("アンパンマン", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(final MaterialDialog dialog, final CharSequence input) {
                                if (input.length() != 0) {

                                    String[] items = {"公式", "クリエイターズ"};

                                    new AlertDialog.Builder(StoreActivity.this)
                                            .setTitle("確認")
                                            .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    select = which;
                                                }
                                            })
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    String type = null;
                                                    if (select == 0)
                                                        type = "general";
                                                    if (select == 1)
                                                        type = "creater";
                                                    Intent intent = new Intent(StoreActivity.this, StoreSearchActivity.class);
                                                    intent.putExtra("type", type);
                                                    intent.putExtra("word", input.toString());
                                                    startActivity(intent);
                                                }
                                            })
                                            .show();

                                    /*
                                    new MaterialDialog.Builder(StoreActivity.this)
                                            .title("確認")
                                            //.content("どちらで検索しますか")
                                            .items("公式", "クリエイターズ")
                                            .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                                @Override
                                                public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                                    /*
                                                    String type = null;
                                                    if (text.equals("公式"))
                                                        type = "general";
                                                    if (text.equals("クリエイターズ"))
                                                        type = "creater";
                                                    Intent intent = new Intent(StoreActivity.this,StoreSearchActivity.class);
                                                    intent.putExtra("type",type);
                                                    intent.putExtra("word",input.toString());
                                                    startActivity(intent);

                                                    return true;
                                                }
                                            })

                                            .show();
                                    */
                                }

                            }
                        })
                        .show();
            }
        });
    }

}
