package net.mizofumi.llne.Store;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.AbsListView;

import com.etsy.android.grid.StaggeredGridView;

import net.mizofumi.llne.Http.CREATETYPE;
import net.mizofumi.llne.Http.StickerSearch;
import net.mizofumi.llne.Http.StickerSearchImpl;
import net.mizofumi.llne.R;
import net.mizofumi.llne.Stamp;

import java.util.List;

public class StoreSearchActivity extends AppCompatActivity implements StickerSearch {

    String word;
    String type;
    CREATETYPE createtype;
    boolean next;
    String next_url;
    private StoreStampsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_search);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        word = getIntent().getStringExtra("word");
        type = getIntent().getStringExtra("type");
        toolbar.setTitle(word+"の検索結果");
        setSupportActionBar(toolbar);

        StaggeredGridView stickers = (StaggeredGridView)findViewById(R.id.stickers);

        assert stickers != null;
        stickers.setOnScrollListener(new AbsListView.OnScrollListener() {
            String temp;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount != 0 && totalItemCount == firstVisibleItem + visibleItemCount && next) {
                    // 最後尾までスクロールした上で、次のページが存在する場合の処理
                    if (this.temp == null || !this.temp.equals(next_url)){
                        temp = next_url;
                        new StickerSearchImpl(StoreSearchActivity.this,createtype, next_url,StoreSearchActivity.this).start();
                    }
                }
            }
        });

        adapter = new StoreStampsAdapter(this);
        stickers.setAdapter(adapter);
        if (type.equals("general"))
            createtype = CREATETYPE.GENERAL;
        if (type.equals("creater"))
            createtype = CREATETYPE.USER;

        new StickerSearchImpl(this, word,createtype,this).start();
    }

    @Override
    public void onGetdata(List<Stamp> stamps, boolean next, String next_url) {
        for (Stamp stamp : stamps){
            adapter.addStamsp(stamp);
            adapter.notifyDataSetChanged();
        }
        this.next = next;
        this.next_url = next_url;
        System.out.println(next);
        if (next_url!=null)

            System.out.println(next_url);
    }
}
