package net.mizofumi.llne.Store;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import net.mizofumi.llne.R;
import net.mizofumi.llne.StampImage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizof on 2016/04/05.
 */
public class SampleStampListAdapter extends BaseAdapter {


    Context context;
    LayoutInflater layoutInflater;
    List<String> stampImages = new ArrayList<>();

    public SampleStampListAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addImage(String data){
        stampImages.add(data);
    }

    @Override
    public int getCount() {
        return stampImages.size();
    }

    @Override
    public Object getItem(int position) {
        return stampImages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.stamplist,parent,false);
        ImageView stamp = (ImageView)view.findViewById(R.id.stamp);

        byte[] bytes = Base64.decode(stampImages.get(position), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        stamp.setImageBitmap(decodedByte);
        return view;
    }

}
