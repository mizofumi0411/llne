package net.mizofumi.llne;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * Created by mizof on 2016/04/04.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        ActiveAndroid.dispose();
    }
}
