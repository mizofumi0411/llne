package net.mizofumi.llne;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import net.mizofumi.llne.Storage.ImageManager;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by mizof on 2016/04/04.
 */
public class StampListAdapter extends BaseAdapter {

    Context context;
    LayoutInflater layoutInflater;
    List<StampImage> stampImages;

    public StampListAdapter(Context context, List<StampImage> stampImages) {
        this.context = context;
        this.stampImages = stampImages;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return stampImages.size();
    }

    @Override
    public Object getItem(int position) {
        return stampImages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final GestureDetector gestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, stampImages.get(position).image_url);
                context.startActivity(intent);
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        gestureDetector.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                Log.d("StampListAdapter","onSingleTapConfirmed");
                ClipData.Item item = new ClipData.Item(stampImages.get(position).image_url);
                String[] mimeType = new String[1];
                mimeType[0] = ClipDescription.MIMETYPE_TEXT_URILIST;
                ClipData cd = new ClipData(new ClipDescription("text_data", mimeType), item);
                ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setPrimaryClip(cd);
                Toast.makeText(context, stampImages.get(position).image_url + "をコピーしました", Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                String image = stampImages.get(position).image;
                Intent intent = new Intent(context,StampPreviewActivity.class);
                intent.putExtra("image",image);
                intent.putExtra("url",stampImages.get(position).image_url);
                context.startActivity(intent);
                return false;
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                return false;
            }
        });
        View view = layoutInflater.inflate(R.layout.stamplist, parent, false);
        ImageView stamp = (ImageView)view.findViewById(R.id.stamp);

        try {
            stamp.setImageBitmap(ImageManager.loadBitmap(context,stampImages.get(position).image));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });

        return view;
    }

}
