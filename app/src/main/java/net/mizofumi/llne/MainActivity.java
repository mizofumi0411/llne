package net.mizofumi.llne;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.afollestad.materialdialogs.MaterialDialog;

import net.mizofumi.llne.Http.StickerGet;
import net.mizofumi.llne.Http.StickerGetDBImpl;
import net.mizofumi.llne.Store.StoreActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private StickerListAdapter adapter;
    private View view1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        view1 = (findViewById(R.id.contentView));
        listView = (ListView)view1.findViewById(R.id.listView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                startActivity(new Intent(MainActivity.this,StoreActivity.class));
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getLocalClassName(),"onResume");
        List<Stamp> stamps = new Select().from(Stamp.class).orderBy("Rating DESC").execute();
        adapter = new StickerListAdapter(this,stamps);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_store) {
            startActivity(new Intent(this,StoreActivity.class));
            return true;
        }

        if (id == R.id.action_urladd){
            new MaterialDialog.Builder(MainActivity.this)
                    .title("URLを入力してください")
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .input("https://store.line.me/stickershop/product/1979/ja", "", new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(final MaterialDialog dialog, CharSequence input) {
                            if (input.length() != 0) {

                                final MaterialDialog progressDialog = new MaterialDialog.Builder(MainActivity.this)
                                        .title("処理中")
                                        .content("スタンプ情報を取得しています…")
                                        .progress(false,100)
                                        .show();

                                StickerGetDBImpl stickerGet = new StickerGetDBImpl(MainActivity.this,input.toString(), new StickerGet() {
                                    @Override
                                    public void onPreExec() {

                                    }

                                    @Override
                                    public void onPostExec(final Stamp stamp) {
                                        progressDialog.dismiss();
                                        adapter.addStamps(stamp);
                                        adapter.notifyDataSetChanged();
                                        Snackbar.make(view1, "スタンプ情報を更新しました", Snackbar.LENGTH_LONG)
                                                .setAction("Action", null).show();
                                    }

                                    @Override
                                    public void doBackground(final int now, final int max) {
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                float p = ((float) now / (float) max) * 100;
                                                progressDialog.setContent("スタンプ情報を取得しています…\n" + now + "/" + max);
                                                progressDialog.setProgress((int) p);
                                                Log.d("Progress", now + "/" + max);
                                            }
                                        });
                                    }
                                });
                                stickerGet.start();
                            }

                        }
                    })
                    .show();

        }
        return super.onOptionsItemSelected(item);
    }
}
