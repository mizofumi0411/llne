package net.mizofumi.llne;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by mizof on 2016/04/04.
 */
@Table(name = "StampImages")
public class StampImage extends Model{

    @Column(name = "StampId")
    public long stampid;

    @Column(name = "Image")
    public String image;

    @Column(name = "ImageUrl")
    public String image_url;

}
