package net.mizofumi.llne.Storage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by mizof on 2016/04/05.
 */
public class ImageManager {

    /*
    String fileName = "sample.jpg";  // "data/data/[パッケージ名]/files/sample.jpg" になる
    Bitmap bitmap = loadBitmap(fileName, this);  //this は起動した Activity が良い(Context)
     */
    public static Bitmap loadBitmap(Context context,String filename) throws FileNotFoundException {
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(context.openFileInput(filename));
            return BitmapFactory.decodeStream(bis);
        } finally {
            try {
                bis.close();
            } catch (Exception e) {
                //IOException, NullPointerException
            }
        }
    }

    /*
    String fileName = "sample.png";  // "data/data/[パッケージ名]/files/sample.png" となる
    saveBitmap(fileName, bitmap, this);  //bitmap は保存する画像/this は起動した Activity が良い(Context)
     */
    public static final boolean saveBitmap(String fileName, Bitmap bitmap, Context context) throws IOException {
        BufferedOutputStream bos = null;
        Bitmap tmp = null;
        try {
            bos = new BufferedOutputStream(context.openFileOutput(fileName, Context.MODE_PRIVATE)); //他アプリアクセス不可
            tmp = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            return tmp.compress(Bitmap.CompressFormat.PNG, 100, bos);
        } finally {
            if (tmp != null) {
                tmp.recycle();
                tmp = null;
            }
            try {
                bos.close();
            } catch (Exception e) {
                //IOException, NullPointerException
            }
        }
    }
}
