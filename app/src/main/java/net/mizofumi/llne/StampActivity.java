package net.mizofumi.llne;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.etsy.android.grid.StaggeredGridView;

import net.mizofumi.llne.Storage.ImageManager;

import java.io.FileNotFoundException;
import java.util.List;

public class StampActivity extends AppCompatActivity {

    private Stamp stamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stamp);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView preview = (ImageView)findViewById(R.id.preview);
        TextView title = (TextView)findViewById(R.id.title);
        TextView detail = (TextView)findViewById(R.id.detail);
        RatingBar rate = (RatingBar)findViewById(R.id.rate);


        StaggeredGridView staggeredGridView = (StaggeredGridView)findViewById(R.id.gridView);

        long id = getIntent().getLongExtra("id", -1);
        if (id == -1){
            Toast.makeText(this,"データがありません",Toast.LENGTH_SHORT).show();
            finish();
        }

        stamp = new Select().from(Stamp.class).where("id = ?", id).executeSingle();
        List<StampImage> stampImages = new Select().from(StampImage.class).where("stampid = ?", id).execute();
        assert title != null;
        assert detail != null;
        assert rate != null;
        title.setText(stamp.title);
        detail.setText(stamp.detail);
        rate.setRating(stamp.rate);
        rate.setIsIndicator(false);
        rate.setStepSize((float) 0.5);
        rate.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                stamp.rate = ratingBar.getRating();
                stamp.save();
            }
        });

        try {
            preview.setImageBitmap(ImageManager.loadBitmap(StampActivity.this,stampImages.get(0).image));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        StampListAdapter adapter = new StampListAdapter(this,stampImages);
        staggeredGridView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_stamp,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_share){
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, stamp.source);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
