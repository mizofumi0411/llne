package net.mizofumi.llne;

import android.graphics.Bitmap;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizof on 2016/04/04.
 */
@Table(name = "Stamps")
public class Stamp extends Model{

    @Column(name = "source")
    public String source;

    @Column(name = "Title")
    public String title;

    @Column(name = "Detail")
    public String detail;

    @Column(name = "Rating")
    public float rate;

    @Column(name = "Preview")
    public String preview;

    @Column(name = "Createtype")
    public String createtype;
}
