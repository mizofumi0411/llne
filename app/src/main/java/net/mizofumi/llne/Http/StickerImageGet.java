package net.mizofumi.llne.Http;

import java.util.List;

/**
 * Created by mizof on 2016/04/05.
 */
public interface StickerImageGet {

    void onGet(List<String> images);

}
