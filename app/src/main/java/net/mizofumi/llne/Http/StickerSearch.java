package net.mizofumi.llne.Http;

import net.mizofumi.llne.Stamp;

import java.util.List;

/**
 * Created by mizof on 2016/04/04.
 */
public interface StickerSearch {

    void onGetdata(List<Stamp> stamps, boolean next, String next_url);

}
