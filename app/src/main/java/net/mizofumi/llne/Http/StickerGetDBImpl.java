package net.mizofumi.llne.Http;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.mizofumi.llne.Stamp;
import net.mizofumi.llne.StampImage;
import net.mizofumi.llne.Storage.ImageManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by mizof on 2016/04/04.
 */
public class StickerGetDBImpl extends Thread implements StickerGet {

    Context context;
    String url;
    StickerGet listener;

    public StickerGetDBImpl(String url) {
        this.url = url;
    }

    public StickerGetDBImpl(Context context,String url, StickerGet listener){
        this.context = context;
        this.url = url;
        this.listener = listener;
    }

    @Override
    public void onPreExec() {
        listener.onPreExec();
    }

    @Override
    public void onPostExec(Stamp stamp) {
        listener.onPostExec(stamp);
    }

    @Override
    public void doBackground(int now, int max) {
        listener.doBackground(now,max);
    }

    @Override
    public void run() {
        super.run();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onPreExec();
            }
        });

        final Stamp stamp = new Stamp();
        stamp.source = url;
        try {
            Document document = Jsoup.connect(url)
                    .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36")
                    .get();
            stamp.title = document.getElementsByClass("mdCMN08Ttl").text();
            stamp.detail = document.getElementsByClass("mdCMN08Desc").text();
            stamp.rate = 0;
            Elements mdCMN09Image = document.getElementsByClass("mdCMN09Image");
            int now = 0;
            doBackground(0, mdCMN09Image.size());
            stamp.save();
            for (Element e : mdCMN09Image){
                doBackground(now, mdCMN09Image.size());

                String a = e.attr("style").split(" ")[4].replaceAll("background-image:url\\(", "").replaceAll("\\);", "");
                String filename = a.split("/")[a.split("/").length-1].split("\\.")[0];
                Log.d("SticlerGetDBImpl",filename);

                StampImage stampImage = new StampImage();
                stampImage.stampid = stamp.getId();
                stampImage.image = filename;
                stampImage.image_url = e.attr("style").split(" ")[4].replaceAll("background-image:url\\(", "").replaceAll("\\);", "");

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(e.attr("style").split(" ")[4].replaceAll("background-image:url\\(", "").replaceAll("\\);", ""))
                        .build();
                Response response = client.newCall(request).execute();
                response.body().byteStream();
                Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());

                ImageManager.saveBitmap(filename,bitmap,context);
                stampImage.save();
                now++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onPostExec(stamp);
            }
        });
    }
}
