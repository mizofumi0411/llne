package net.mizofumi.llne.Http;

import net.mizofumi.llne.Stamp;

/**
 * Created by mizof on 2016/04/04.
 */
public interface StickerGet {

    void onPreExec();
    void onPostExec(Stamp stamp);
    void doBackground(int now,int max);
}
