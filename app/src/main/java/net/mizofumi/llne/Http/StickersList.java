package net.mizofumi.llne.Http;

import net.mizofumi.llne.Stamp;

import java.util.List;

/**
 * Created by mizof on 2016/04/05.
 */
public interface StickersList {
    void getStickers(List<Stamp> stamps);
}
