package net.mizofumi.llne.Http;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.mizofumi.llne.Stamp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizof on 2016/04/05.
 */
public class StickerSearchImpl extends Thread implements StickerSearch {

    Context context;
    String url;
    String baseurl;
    String word;
    StickerSearch listener;
    CREATETYPE createtype;

    public StickerSearchImpl(Context context,String word, CREATETYPE createtype,StickerSearch listener) {
        this.context = context;
        try {
            this.word = URLEncoder.encode(word, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.createtype = createtype;
        this.listener = listener;
    }

    public StickerSearchImpl(Context context,CREATETYPE createtype,String url,StickerSearch listener) {
        this.context = context;
        this.url = url;
        this.listener = listener;
        this.createtype = createtype;
    }

    @Override
    public void run() {
        super.run();
        if (url == null && createtype != null && word != null){
            url = "";
            if (createtype == CREATETYPE.GENERAL){
                url = "https://store.line.me/stickershop/search/ja?q="+word+"&page=1";
            }
            if (createtype == CREATETYPE.USER){
                url = "https://store.line.me/stickershop/search/creators/ja?q="+word+"&page=1";
            }
        }

        if (createtype == CREATETYPE.GENERAL){
            baseurl = "https://store.line.me/stickershop/search/ja";
        }
        if (createtype == CREATETYPE.USER){
            baseurl = "https://store.line.me/stickershop/search/creators/ja";
        }

        List<Stamp> stamps = new ArrayList<>();

        try {
            Document document = Jsoup.connect(url)
                    .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36")
                    .get();

            Elements mdCMN02Li = document.getElementsByClass("mdCMN02Li");
            for (Element element : mdCMN02Li){
                Stamp stamp = new Stamp();
                stamp.source = "https://store.line.me/"+element.getElementsByTag("a").attr("href");
                stamp.title = element.getElementsByClass("mdCMN05Ttl").text();
                if (createtype == CREATETYPE.GENERAL)
                    stamp.createtype = "general";
                if (createtype == CREATETYPE.USER)
                    stamp.createtype = "creater";


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(element.getElementsByTag("img").attr("src"))
                        .build();
                Response response = client.newCall(request).execute();
                response.body().byteStream();
                Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                stamp.preview = encoded;
                stamps.add(stamp);
            }
            boolean aa = !document.getElementsByClass("mdCMN14Next").isEmpty();
            String next_url = "";
            if (aa){
                next_url = baseurl + document.getElementsByClass("mdCMN14Next").attr("href");
                onGetdata(stamps,aa,next_url);
            }else {
                onGetdata(stamps,false,null);
            }

        } catch (IOException e) {
            e.printStackTrace();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "検索結果がありません", Toast.LENGTH_SHORT).show();
                }
            });
        }


    }

    @Override
    public void onGetdata(final List<Stamp> stamps, final boolean next, final String next_url) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (listener != null) listener.onGetdata(stamps,next,next_url);
            }
        });
    }
}
