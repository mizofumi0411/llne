package net.mizofumi.llne.Http;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.mizofumi.llne.Stamp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizof on 2016/04/05.
 */
public class StickersListImpl extends Thread implements StickersList {

    Context context;
    StickersList listener;

    public StickersListImpl(Context context,StickersList listener){
        this.context = context;
        this.listener = listener;
    }

    @Override
    public void run() {
        super.run();

        final List<Stamp> stamps = new ArrayList<>();

        try {
            getData("general","https://store.line.me/stickershop/showcase/new/ja", stamps);
            getData("creater", "https://store.line.me/stickershop/showcase/new_creators/ja", stamps);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.getStickers(stamps);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected void getData(String type,String url,List<Stamp> stamps) throws IOException {
        Document document = Jsoup.connect(url)
                .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36")
                .get();
        Elements mdCMN02Li = document.getElementsByClass("mdCMN02Li");
        for (Element element : mdCMN02Li){
            Stamp stamp = new Stamp();
            stamp.source = "https://store.line.me/"+element.getElementsByTag("a").attr("href");
            stamp.title = element.getElementsByClass("mdCMN05Ttl").text();
            stamp.createtype = type;

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(element.getElementsByTag("img").attr("src"))
                    .build();
            Response response = client.newCall(request).execute();
            response.body().byteStream();
            Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            stamp.preview = encoded;
            stamps.add(stamp);
        }
    }

    @Override
    public void getStickers(List<Stamp> stamps) {

    }
}
